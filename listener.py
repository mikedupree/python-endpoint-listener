import sys
from flask import Flask, request, abort

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET', 'PUT', 'DELETE', 'PATCH'])
def webhook():
  print("webhook"); sys.stdout.flush()
  print(request.get_data())
  if request.method in ['POST', 'PATCH']:
    print(request.json)
    return '', 200
  else:
    print(request.json)
    abort(400)


if __name__ == '__main__':
  app.run(host = "0.0.0.0")