# Python listener

## Log body data from your requests

### How to

* copy the `listener.py`

* run `python listener.py` this will bind to your address on port 5000

* (optional) setup nginx config to point a url to your listener script
